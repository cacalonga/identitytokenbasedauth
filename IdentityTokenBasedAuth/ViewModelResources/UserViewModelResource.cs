﻿using IdentityTokenBasedAuth.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityTokenBasedAuth.ViewModelResources
{
	public class UserViewModelResource
	{
        [Required(ErrorMessage = "Kullanıcı ismi gerekldir.")]
        public string UserName { get; set; }

        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Email adresi gereklidir.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifreniz gereklidir.")]
        public string Password { get; set; }

        public string BirthDay { get; set; }

        public string Picture { get; set; }

        public string City { get; set; }

        public Gender Gender { get; set; }
    }
}
