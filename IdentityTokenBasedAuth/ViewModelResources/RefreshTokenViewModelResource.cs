﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityTokenBasedAuth.ViewModelResources
{
	public class RefreshTokenViewModelResource
	{
		public string RefreshToken { get; set; }
	}
}
