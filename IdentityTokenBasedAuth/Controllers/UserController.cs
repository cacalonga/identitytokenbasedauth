﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Models;
using IdentityTokenBasedAuth.ViewModelResources;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IdentityTokenBasedAuth.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	[Authorize]
	public class UserController : ControllerBase, IActionFilter
	{
		private readonly IUserService userService;
		public UserController(IUserService userService)
		{
			this.userService = userService;
		}

		public void OnActionExecuted(ActionExecutedContext context)
		{
			//throw new NotImplementedException();
		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			context.ModelState.Remove("Password");
		}
		[HttpGet]
		public async Task<IActionResult> GetUser()
		{
			AppUser user = await userService.GetUserByUserName(User.Identity.Name);
			return Ok(user.Adapt<UserViewModelResource>());
		}
		[HttpPost]
		public async Task<IActionResult> UpdateUser(UserViewModelResource userViewModel)
		{
			var response = await userService.UpdateUser(userViewModel, User.Identity.Name);
			if (response.Success)
			{
				return Ok(response.Extra);
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}

		}
		[HttpPost]
		public async Task<IActionResult> UploadUserPicture(IFormFile picFile)
		{
			var fileName = Guid.NewGuid().ToString() + Path.GetExtension(picFile.FileName);

			var path = Path.Combine(Directory.GetCurrentDirectory() + "wwwroot/UserPictures", fileName);

			using (var stream = new FileStream(path, FileMode.Create))
			{
				await picFile.CopyToAsync(stream);
			}

			var result = new { path = "https://" + Request.Host + "/UserPictures/" + fileName };
			var response = await userService.UploadUserPicture(result.path, User.Identity.Name);
			if (response.Success)
			{
				return Ok(path);
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}
		}
	}
}
