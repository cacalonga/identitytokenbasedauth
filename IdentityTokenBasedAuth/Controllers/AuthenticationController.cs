﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityTokenBasedAuth.Domain.Responses;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.ViewModelResources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IdentityTokenBasedAuth.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class AuthenticationController : ControllerBase
	{

		private readonly IAuthenticationService authenticationService;
		public AuthenticationController(IAuthenticationService service)
		{
			this.authenticationService = service;
		}

		[HttpGet]
		public ActionResult IsAuthentication()
		{
			return Ok(User.Identity.IsAuthenticated);
		}

		[HttpPost]
		public async Task<IActionResult> SignUp(UserViewModelResource userViewModelResource)
		{
			BaseResponse<UserViewModelResource> response = await authenticationService.SignUp(userViewModelResource);

			if (response.Success)
			{
				return Ok(response.Extra);
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}

		}

		[HttpPost]
		public async Task<IActionResult> SignIn(SignInViewModelResource signInViewModelResource)
		{
			var response = await authenticationService.SignIn(signInViewModelResource);
			if (response.Success)
			{
				return Ok(response.Extra);
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}
		}

		[HttpPost]
		public async Task<IActionResult> TokenByRefreshToken(RefreshTokenViewModelResource refreshTokenViewModelResource)
		{
			var response = await authenticationService.CreateAccessTokenByRefreshToken(refreshTokenViewModelResource);
			if (response.Success)
			{
				return Ok(response.Extra);
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}
		}
		[HttpDelete]
		public async Task<IActionResult> RevokeRefreshToken(RefreshTokenViewModelResource refreshTokenViewModelResource)
		{
			var response = await authenticationService.RewokeRefreshToken(refreshTokenViewModelResource);

			if (response.Success)
			{
				return Ok();
			}
			else
			{
				return BadRequest(response.ErrorMessage);
			}
		}
	}
}