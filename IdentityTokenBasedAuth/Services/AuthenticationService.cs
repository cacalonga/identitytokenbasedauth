﻿using IdentityTokenBasedAuth.Domain.Responses;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Models;
using IdentityTokenBasedAuth.Security.Token;
using IdentityTokenBasedAuth.ViewModelResources;
using Mapster;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityTokenBasedAuth.Services
{
	public class AuthenticationService : BaseService, IAuthenticationService
	{
		private readonly ITokenHandler tokenHandler;
		private readonly CustomTokenOptions customTokenOptions;
		private readonly IUserService userService;
		public AuthenticationService(UserManager<AppUser> userManager, SignInManager<AppUser> signIn, RoleManager<AppRole> roleManager, ITokenHandler tokenHandler, IUserService userService, IOptions<CustomTokenOptions> options) : base(userManager, signIn, roleManager)
		{
			this.tokenHandler = tokenHandler;
			this.userService = userService;
			this.customTokenOptions = options.Value;
		}

		public async Task<BaseResponse<AccessToken>> CreateAccessTokenByRefreshToken(RefreshTokenViewModelResource refreshTokenViewModel)
		{
			var userCliam = await userService.GetUserByRefreshToken(refreshTokenViewModel.RefreshToken);
			if (userCliam.Item1 != null)
			{
				AccessToken accessToken = tokenHandler.CreateAccessToken(userCliam.Item1);
				Claim refreshTokenClaim = new Claim("refreshToken", accessToken.RefreshToken);
				Claim refreshTokenEndDateClaim = new Claim("refreshTokenEndDate", DateTime.Now.AddMinutes(customTokenOptions.RehreshTokenExpiretion).ToString());
				await userManager.ReplaceClaimAsync(userCliam.Item1, userCliam.Item2[0], refreshTokenClaim);
				await userManager.ReplaceClaimAsync(userCliam.Item1, userCliam.Item2[1], refreshTokenEndDateClaim);
				return new BaseResponse<AccessToken>(accessToken);
			}
			else
			{
				return new BaseResponse<AccessToken>("Böyle bir refresh tokena sahip kullanıcı yoktur.");
			}
		}

		public async Task<BaseResponse<AccessToken>> RewokeRefreshToken(RefreshTokenViewModelResource refreshTokenViewModel)
		{
			bool result = await userService.RevokeRefreshToken(refreshTokenViewModel.RefreshToken);
			if (result)
			{
				return new BaseResponse<AccessToken>(new AccessToken());
			}
			else
			{
				return new BaseResponse<AccessToken>("refreshToken bulunamadı");
			}
		}

		public async Task<BaseResponse<AccessToken>> SignIn(SignInViewModelResource signInViewModel)
		{

			AppUser user = await userManager.FindByEmailAsync(signInViewModel.Email);
			if (user != null)
			{
				bool isUser = await userManager.CheckPasswordAsync(user, signInViewModel.Password);
				if (isUser)
				{
					AccessToken accessToken = tokenHandler.CreateAccessToken(user);

					Claim refreshTokenClaim = new Claim("refreshToken", accessToken.RefreshToken);
					Claim refreshTokenEndDateClaim = new Claim("refreshTokenEndDate", DateTime.Now.AddMinutes(customTokenOptions.RehreshTokenExpiretion).ToString());

					List<Claim> refreshClaimList = userManager.GetClaimsAsync(user).Result.Where(x => x.Type.Contains("refreshToken")).ToList();

					if (refreshClaimList.Any())
					{
						await userManager.ReplaceClaimAsync(user, refreshClaimList[0], refreshTokenClaim);
						await userManager.ReplaceClaimAsync(user, refreshClaimList[1], refreshTokenEndDateClaim);
					}
					else
					{
						await userManager.AddClaimsAsync(user, new[] { refreshTokenClaim, refreshTokenEndDateClaim });
					}
					return new BaseResponse<AccessToken>(accessToken);
				}
			}
			return new BaseResponse<AccessToken>("Email veya Şifre yanlış");
		}

		public async Task<BaseResponse<UserViewModelResource>> SignUp(UserViewModelResource userViewModel)
		{
			AppUser user = new AppUser { UserName = userViewModel.UserName, Email = userViewModel.Email };

			IdentityResult result = await userManager.CreateAsync(user,userViewModel.Password);
			if (result.Succeeded)
			{
				return new BaseResponse<UserViewModelResource>(user.Adapt<UserViewModelResource>());
			}
			else
			{
				return new BaseResponse<UserViewModelResource>(result.Errors.First().Description);
			}
		}
	}
}
